%include "xen/elfnote.inc"
%include "x86/asm_macros.mac"
global start

section .text
bits 32
start:
  ;print OK to the screen
  mov dword [0xb8f00], 0x2f4b2f4f
  mov dword [0xb8f04], 0x2f592f41
  mov eax, 0xffffffff
loop:
  ; Code to loop here
  dec eax
  jnz loop
  mov dword [0xb8f00], 0x2f412f48
  mov dword [0xb8f04], 0x2f542f4c
  ;hlt
  mov ax, 0x1000
  mov ax, ss
  mov sp, 0xf000
  mov ax, 0x5307
  mov bx, 0x0001
  mov cx, 0x0003
  int 0x15

section .note
elf_notes:
  elfnote "Xen",XEN_ELFNOTE_GUEST_OS,"my-os"
  elfnote "Xen",XEN_ELFNOTE_GUEST_VERSION,"0.1"
  elfnote "Xen",XEN_ELFNOTE_LOADER,"generic"
  elfnote "Xen",XEN_ELFNOTE_XEN_VERSION,"xen-3.0"
